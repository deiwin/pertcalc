'use strict';

// namespace for custom directives
var myAppl = angular.module('PertCalc', []);

myAppl.directive('pertTutorial', function () {
    return {
        restrict: 'E',
        templateUrl: 'partials/tutorial.html'
    };
});

myAppl.directive('taskArea', function () {
    return {
        restrict: 'E',
        templateUrl: 'partials/task-area.html'
    };
});

myAppl.directive('customFooter', function () {
    return {
        restrict: 'E',
        templateUrl: 'partials/footer.html'
    };
});

myAppl.directive('googleSignin', function () {
    return {
        restrict: 'E',
        templateUrl: 'partials/sign-in.html'
    };
});


