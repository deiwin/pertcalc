'use strict';

/* Controllers */

function Calculator($scope, $window) {

    $scope.tasks = [];

    $scope.change = function() {
        // handle the value changes
        $scope.totalDuration = 0;
        $scope.totalUncertainty = 0;
        $scope.uncertaintySum = 0;
        for (var i = 0; i < $scope.tasks.length; i++) {
            $scope.tasks[i].total = ($scope.tasks[i].optimistic
                + 4*$scope.tasks[i].nominal
                + $scope.tasks[i].pessimistic) / 6;
            $scope.tasks[i].uncertainty = ($scope.tasks[i].pessimistic - $scope.tasks[i].optimistic) / 6;
            $scope.totalDuration += $scope.tasks[i].total;
            $scope.uncertaintySum += $window.Math.pow($scope.tasks[i].uncertainty, 2);
        }
        $scope.totalUncertainty = $window.Math.sqrt($scope.uncertaintySum);
    };

    $scope.addTask = function () {
        $scope.tasks.push({name: $scope.taskName, opt: $scope.optimistic, nom: $scope.nominal, pes: $scope.pessimistic});
        $scope.taskName = '';
    };

    $scope.tasksExist = function () {
        return $scope.tasks.length > 0;
    };

    $scope.removeTask = function (idx) {
        if (~idx) $scope.tasks.splice(idx, 1);
        $scope.change();
    }

    $scope.removeAllTasks = function () {
        $scope.tasks = [];
    }
}